# Silos

The year is 2562ad.  The surface of the world is scorched black and covered in
ash.  Fierce storm winds punish the atmosphere at speeds upto 600 miles per
hour; and with the ash and sedement in the air, a creature could have it's flesh
stripped from its bones in 20 seconds.  Because of these storms, most of the
earth's surface has been flattened as mountains have eroded to little more than
foothills in the past four centuries.  The sun is shrouded perpetually by thick
black clouds of carbon, casting the earth in a never ending night.

Lightning storms ignite the dry, carbon saturated soil with ease, causing
wildfires that blaze brightly and scream across the surface of the world.

Survival on the surface is impossible even if only for a few hours.

There is no food, no water and the air is nearly unbreathable.
The wind, fires, and freezing cold threaten to kill you at every moment.

Desperate for survival, humanity has been driven far underground into cities we
call Silos.  They vary in size and shape depending on the needs, and engineering
prowess of the survivors who built them.  Some are connected by hundreds of
miles of tunnel which are dangerous to pass through.  Being poorly maintained
there are often collapses along the route, and opportunistic bandits we just
call "Wormers".  Alamo is connected by two such routes to other silos.  To the
north about 80 miles is "ATX", and far to the west about 550 miles is "Chulo".
We don't know much about Chulo because the tunnel that leads there has been
unusable for about 150 years.  ATX however is like our more technologically
advanced sister silo.  The Alamo shares its food and mined resources with ATX in
exchange for tech.  30 years ago our two silos finished a mag-lev transportation
system that connects us to them and has been running non-stop since it opened.

Alamo has 37 million people.  ATX has 19 million.

Electricity is generated geothermally or via the turbine generators on the
surface.

We keep track of time using modules on the surface called Clocks. Clocks observe
the sky at all times, and when there is a break in the carbon clouds, logs the
position of the sun or the stars.  Alamo has 7 modules at the surface, ATX has
11, and we have teams of people trained and dedicated to keeping them
synchronized.

ATX laid a large fiber optic line between them and us to use for communication
and a form of the archaic internet we just call "Mesh" - though the internet
doesn't have many nodes on the network so there isn't much that is useful.

Under nearly a mile of earth lies your home silo, "Alamo", which historians say
is beneath the former San Antonio, TX, USA.  The silo is a network of concrete
tubes that has turned into pretty complex maze of nearly identical tunnels.
Fortunately not much new construction has occurred within the last 100 years, so
you know your way around.

Our main source of food is whatever we can grow - a lot of it is genetically
modified from the labs at ATX.  Potatoes spliced with lettuce-like greens, corn
with very fiberous stocks and leafs used for cloth, as well as cotton and a
wheat strain that can mature and be harvested in two months.  Football field
sized concrete pads covered in chemically enhanced soil rise in stacks in a
sector of the silo called 53 or "Farms".  ATX has been able to produce lighting
fixtures based on primitive led that have adjustable frequency and extremely low
energy usage.  These fixtures are used for giving light to the whole silo as
well as providing the appropriate frequency of light for photosynthesis in the
Farms.

Water is pulled from natural springs and underground waterways.

A residence called a "Box" are 20x20x20 concrete cells that sit in stacked
columns called "Beams", usually 10 high.  People paint the exteriors to have
some uniqueness, but available colors are few with rare colors being quite
expensive to produce and are only imported from ATX.  Furniture is made of stone
and concrete, while fabric furniture is made from dried and processed corn
byproduct.

People get around in mag-lev pods, a public transport system.  A pod holds 6
people, or 145 cubic ft of internal cargo space (like a Ford Transit van).  Pod
traffic is controlled from a central hub and is completely automated by AI.
There is one pod station for every 10 Beams (or 100 Boxes [houses]).

"Packets" are a digital crypto currency that has been created and utilized since
the silo was considered "stable" around 100 years after it was first created.

There are a few occupations that keep the silos running, and keep humanity
human. Farming, textile factories, mineral mining, all disciplines of
engineering, retail and services, and security.  All farming in Alamo happens in
the Farms and factories in the Mills.  Retail and services are spread throughout
the silo in locations near Beams. The mines extend out in all directions from
the silo and when expansion is necessary, the escavated space is utilized.  The
strata around the Alamo is rich with copper, iron ore, zinc, basalt and
aluminum.  Silver and gold can also be found, but ATX pays a premium for it and
the Alamo has little use for it outside what ATX produces with it, so nearly all
of it goes to them.  Security is mixed between private, corporate and
government.  All weapons are only legal if they are non-lethal, so security
agents carry shock batons and the ranged P1A tazer, we call Scorchers.

Racism in the historical sense is not present in the silo - but humanity has
changed in the past 500 years.  The silo is socially divided into three groups,
"Prims", "Mechs" and "Muts".  Prims are only called so by mechs and muts, the
term being short for primatives.  The are purely human, and as such, the
elitists among them call themselves "Pure".  Prims make up about 50% of Alamo's
population.  Mechs are humans that have been mechanically enhanced in some way,
whether voluntarily or by need for prosthesis. They have enhanced speed,
strength and other athletic abilities.  Mechs tend to also have specialized
enchancements for whatever their occupation calls for. They represent about 30%
of the population.  Muts are mutated humans. Long ago, shortly after the carbon
storms drove humanity underground, humans sought out ways to cope with their new
environment.  So scientist began biologically engineering human genetics with
subsurface dwelling creatures.  They have far keener senses than prims and eyes
well suited for the dark. Today, that means most of them wear shades when in
well lit public places.  The first muts were outcasts, but over time their
population grew so they represent nearly 20% of the population of Alamo.  Their
appearance differs from prims however, larger eyes that are black, completely
hairless and nearly translucent pigmentless skin.

## Current Issues
Wormers
Energy/Food/Water consumption
Corruption
Racial conflicts:
	- Mutated humans called "Muts"
	- Mechanically enhanced humans called "Mechs"
	- Regular humans are called "Primitives" or "Prims"

## Impending Issues
Something in the tunnel route from Chulo
ATX labs created a contagion or weapon
Another silo has tunneled into a connecting route with the Alamo
Someone found the Alamo from the surface
Creatures have formed from radiation on the surface and made their way down

## Long Plots

