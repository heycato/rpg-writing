I was sitting here writing a recap of last session and I realized I forgot to
update you on the events.

- Sasuto used Phase Shift to get the party out of the Shadowfell.
	- He brought them back to Swallowdown in the Material Plane.
	- Signal, Hanz and Sasuto got really sick and passed out for 20 mins...
- Sasuto contacted Nueleth that evening to report what happened in the mines.
	- He was told of what happened with the eldership in Bleigh, and was
		requested to return in the morning.
- Before Sasuto departed, he gave Hanz two scrolls
	- A teleportation circle to match the one at his home in... (wherever he
		lives)
	- A custom Alert spell I'm calling "Remote Alert"
		- Just an Alert spell without a range limit or plane restriction... so you
			can hear it triggered from anywhere.

- Sasuto determined that the rift in the mine was caused by the earthquakes
	that hit the region when the party went though the 9th Gate.
	- The gates are "forbidden" because of their volatility and instability.
	- Just being near enough to a gate to passively draw magic from it can cause
		earthquakes, or even rift collapses.

- Yenros returned the next day
	- He let them know what happened in Bleigh from his perspective:

	- reminders:
	"Yenros accompanied Nueleth to her hearing at the Kettledrum's meeting room
	in Bleigh (Same place you were in before when you met the Sund'Arin elders).
	After explaining to the eldership what happened in Swallowdown, she requested
	to be sent back to resume her post and to keep the party who rescued her to
	train and help keep the gate secure.  Yenros and Nueleth left while the
	elders deliberated a verdict.  When he and Nueleth returned to the
	Kettledrum, they found the bar keep had been killed, and the meeting hall
	disheveled and heavily covered with blood, but none of the elders could be
	found.  Yenros found 5 small metal orbs, black with an iridescent purple,
	that he recognized as being of Cerin make, though neither he nor Nueleth
	knows what they are.  Nueleth tried to use Sending to contact them, and when
	that failed attempted to Scry but was magically blocked from seeing them.
	She then started contacting other regents around the world to send Farohts to
	Bleigh to begin the search.  In the mean time she is staying in Bleigh to
	oversee the investigation.  In a few days, Yenros is to return to Ralta to
	relieve Sasuto, so he can help with the search as well."

- To cover you being gone every other session:
	- Yenros has been tasked to "Acting Regent of the 9th Gate"
		- So he'll have to stay close to Swallowdown for the time being.

- Two weeks in game has elapsed.

- Snordleh (the mining town you were sent to) was completely destroyed by the
	explosion from the rift collapsing.

- Earthquakes in the aftermath of the explosion destroyed a few other towns.
	- One major town, Ekavale was hit pretty hard causing a mass exodus.
		- Previous residents of Swallowdown returned.
		- Many people from the towns hit by the earthquake came to Swallowdown to
			start over.

There's other stuff, but the party will catch you up next session.
